﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExifSharp
{
    public partial class Exif
    {
        //
        // 类型声明
        //
        #region Type declarations

        // 
        // 包含EXIF可能的标签名(ID)
        // Contains possible values of EXIF tag names (ID)
        // 
        public enum TagNames : int
        {

            /* TIFF Rev.6.0 Attribute Information */
            //Tags relating to image data structure
            ImageWidth = 0x100,
            ImageHeight = 0x101,
            BitsPerSample = 0x102,
            Compression = 0x103,
            PhotometricInterpretation = 0x106,
            Orientation = 0x112,
            SamplesPerPixel = 0x115,
            PlanarConfiguration = 0x11C,
            YCbCrSubSampling = 0x212,
            YCbCrPositioning = 0x213,
            XResolution = 0x11A,
            YResolution = 0x11B,
            ResolutionUnit = 0x128,
            //Tags relating to recoding offset
            StripOffsets = 0x111,
            RowsPerStrip = 0x116,
            StripBytesCounts = 0x117,
            JPEGInterchangeFormat = 0x201,
            JPEGInterchangeFormatLength = 0x202,
            //Tags relating to image data characteristics
            TransferFuncition = 0x12D,
            WhitePoint = 0x13E,
            PrimaryChromaticities = 0x13F,
            YCbCrCoefficients = 0x211,
            ReferenceBlackWhite = 0x214,
            //Other tags
            DateTime = 0x132,
            ImageDescription = 0x10E,
            Make = 0x10F,
            Model = 0x110,
            Software = 0x131,
            Artist = 0x13B,
            Copyright = 0x8298,

            /* Exif IFD Attribute Information */
            //Tags Relating to Version
            ExifVersion = 0x9000,
            FlashpixVersion = 0xA000,
            //Tag Relating to Image Data Characteristics
            ColorSpace = 0xA001,
            Gamma = 0xA500,
            //Tags Relating to Image Configuration
            ComponentsConfiguration = 0x9101,
            CompressedBitsPerPixel = 0x9102,
            PixelXDimension = 0xA002,
            PixelYDimension = 0xA003,
            //Tags Relating to User Information
            MakerNote = 0x927C,
            UserComment = 0x9286,
            //Tag Relating to Related File Information
            RelatedSoundFile = 0xA004,
            //Tags Relating to Date and Time
            DateTimeOriginal = 0x9003,
            DateTimeDigitized = 0x9004,
            SubSecTime = 0x9290,
            SubSecTimeOriginal = 0x9291,
            SubSecTimeDigitized = 0x9292,
            //Tags Relating to Picture-Taking Conditions
            ExposureTime = 0x829A,
            FNumber = 0x829D,
            ExposureProgram = 0x8822,
            SpectralSensitivity = 0x8824,
            PhotographicSensitivity = 0x8827,
            OECF = 0x8828,
            SensitivityType = 0x8830,
            StandardOutputSensitivity = 0x8831,
            RecommendExposureIndex = 0x8832,
            ISOSpeed = 0x8833,
            ISOSpeedLatitudeyyy = 0x8834,
            ISOSpeedLatitudezzz = 0x8835,
            ShutterSpeedValue = 0x9201,
            ApertureValue = 0x9202,
            BrightnessValue = 0x9203,
            ExposureBiasValue = 0x9204,
            MaxApertureValue = 0x9205,
            SubjectDistance = 0x9206,
            MeteringMode = 0x9207,
            LightSource = 0x9208,
            Flash = 0x9209,
            FocalLength = 0x920A,
            SubjectArea = 0x9214,
            FlashEnergy = 0xA20B,
            SpatialFrequencyResponse = 0xA20C,
            FocalPlaneXResolution = 0xA20E,
            FocalPlaneYResolution = 0xA20F,
            FocalPlaneResolutionUnit = 0xA210,
            SubjectLocation = 0xA214,
            ExposureIndex = 0xA215,
            SensingMethod = 0xA217,
            FileSource = 0xA300,
            SceneType = 0xA301,
            CFAPattern = 0xA302,
            CustomRendered = 0xA401,
            ExposureMode = 0xA402,
            WhiteBalance = 0xA403,
            DigitalZoomRatio = 0xA404,
            FocalLengthIn35mmFilm = 0xA405,
            SceneCaptureType = 0xA406,
            GainControl = 0xA407,
            Contrast = 0xA408,
            Saturation = 0xA409,
            Sharpness = 0xA40A,
            DeviceSettingDescription = 0xA40B,
            SubjectDistanceRange = 0xA40C,
            //Other Tags
            ImageUniqueID = 0xA420,
            CameraOwnerName = 0xA430,
            BodySerialNumber = 0xA431,
            LensSpecification = 0xA432,
            LensMake = 0xA433,
            LensModel = 0xA434,
            LensSerialNumber = 0x435,
         
            // GPS Attribute Information 
            GPSVersionID = 0x0,
            GPSLatitudeRef = 0x1,
            GPSLatitude = 0x2,
            GPSLongitudeRef = 0x3,
            GPSLongitude = 0x4,
            GPSAltitudeRef = 0x5,
            GPSAltitude = 0x6,
            GPSTimeStamp = 0x7,
            GPSSatellites = 0x8,
            GPSStatus = 0x9,
            GPSMeasureMode = 0xA,
            GPSDop = 0xB,
            GPSSpeedRef = 0xC,
            GPSSpeed = 0xD,
            GPSTrackRef = 0xE,
            GPSTrack = 0xF,
            GPSImgDirectionRef = 0x10,
            GPSImgDirection = 0x11,
            GPSMapDatum = 0x12,
            GPSDestLatitudeRef = 0x13,
            GPSDestLatitude = 0x14,
            GPSDestLongitudeRef = 0x15,
            GPSDestLongitude = 0x16,
            GPSDestBearingRef = 0x17,
            GPSDestBearing = 0x18,
            GPSDestDistanceRef = 0x19,
            GPSDestDistance = 0x1A,
            GPSProcessingMethod = 0x1B,
            GPSAreaInformation = 0x1C,
            GPSDateStamp = 0x1D,
            GPSDifferential = 0x1E,
            GPSHPositioningError = 0x1F
        }

        public enum Compressions : short
        {
            Uncompressed = 1,
            JPEGcompression = 6,
            reserved = 255
        }

        public enum PlanarConfigurations : short
        {
            chunkyformat = 1,
            planarformat = 2,
            reserved = 255
        }

        public enum PhotometricInterpretations : short
        {
            RGB = 2,
            YCbCr = 6,
            reserved = 255
        }

        public enum YCbCrPositionings
        {
            Centered = 1,
            Cosited = 2,
            reserved = 255
        }
        // 
        // 方向
        // 图像第0行第0列的真实位置
        // Real position of 0th row and column of picture
        // 
        public enum Orientations : short
        {
            TopLeft = 1,
            TopRight = 2,
            BottomRight = 3,
            BottomLeft = 4,
            LeftTop = 5,
            RightTop = 6,
            RightBottom = 7,
            LeftBottom = 8
        }
        // 
        // 曝光程序
        // Exposure programs
        // 
        public enum ExposurePrograms : short
        {
            Manual = 1,
            Normal = 2,
            AperturePriority = 3,
            ShutterPriority = 4,
            Creative = 5,
            Action = 6,
            Portrait = 7,
            Landscape = 8,
        }
        // 
        // 测光模式
        // Exposure metering modes
        // 
        public enum ExposureMeteringModes : short
        {
            Unknown = 0,
            Average = 1,
            CenterWeightedAverage = 2,
            Spot = 3,
            MultiSpot = 4,
            MultiSegment = 5,
            Partial = 6,
            Other = 255
        }
        // 
        // 快门模式
        // Flash activity modes
        //  
        public enum Flashs : short
        {
            NotFired = 0,
            Fired = 1,
            FiredButNoStrobeReturned = 5,
            FiredAndStrobeReturned = 7,
        }
        // 
        // 光源（白平衡）
        // Possible light sources (white balance)
        // 
        public enum LightSources : short
        {
            Unknown = 0,
            Daylight = 1,
            Fluorescent = 2,
            Tungsten = 3,
            Flash = 10,
            StandardLightA = 17,
            StandardLightB = 18,
            StandardLightC = 19,
            D55 = 20,
            D65 = 21,
            D75 = 22,
            Other = 255
        }
        // 
        // EXIF数据类型
        // EXIF data types
        // 
        public enum ExifDataTypes : short
        {
            UnsignedByte = 1,
            AsciiString = 2,
            UnsignedShort = 3,
            UnsignedLong = 4,
            UnsignedRational = 5,
            SignedByte = 6,
            Undefined = 7,
            SignedShort = 8,
            SignedLong = 9,
            SignedRational = 10,
            SingleFloat = 11,
            DoubleFloat = 12
        }
        // 
        // 有理化
        // Represents rational which is type of some Exif properties
        // 
        public struct Rational
        {
            public Rational(Int32 N, Int32 D)
            {
                Numerator = N;
                Denominator = D;
            }
            public Rational(string Value)
            {
                Numerator = 0;
                Denominator = 1;
                try
                {
                    if (Value.IndexOf('/') > -1)
                    {
                        Numerator = int.Parse(Value.Split('/')[0]);
                        Denominator = int.Parse(Value.Split('/')[1]);
                    }
                    else
                    {
                        Numerator = int.Parse(Value);
                        Denominator = 1;
                    }
                }
                catch { }
            }
            public Int32 Numerator;
            public Int32 Denominator;
            //
            // 转换为字符串表示
            // Converts rational to string representation
            // 
            // Optional, default "/". String to be used as delimiter of components.
            // String representation of the rational.
            // 
            public override string ToString()
            {
                return ToString("/");
            }

            public string ToString(string Delimiter)
            {
                return Numerator + "/" + Denominator;
            }
            // 
            // 转换为双精度浮点数表示
            // Converts rational to double precision real number
            // 
            // The rational as double precision real number.
            //  
            public double ToDouble()
            {
                return (double)Numerator / Denominator;
            }
        }

        #endregion
    }
}
