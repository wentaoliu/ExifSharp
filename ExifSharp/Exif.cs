﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ExifSharp
{
    /// <summary>
    /// Utility class for working with EXIF data in images. 
    /// Provides abstraction for most common data and generic utilities for work with all other. 
    /// </summary>
    public partial class Exif : IDisposable
    {
        private System.Drawing.Bitmap _Image;
        private string _FileName;
        private System.Text.Encoding _Encoding = System.Text.Encoding.UTF8;

        #region Initialize

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="FileName">Name of file to be loaded</param>
        public Exif(string FileName)
        {
            this._FileName = FileName;
            this._Image = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromFile(FileName);
        }

        // 获取或者设置字符串元数据的编码类型
        // Get or set encoding used for string metadata.
        // Encoding used for string metadata
        // Default encoding is UTF-8
        public System.Text.Encoding Encoding
        {
            get
            {
                return this._Encoding;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException();
                this._Encoding = value;
            }
        }

        /// <summary>
        /// Returns copy of bitmap this instance is working on.
        /// </summary>
        /// <returns>copy of bitmap this instance is working on</returns>
        public System.Drawing.Bitmap GetBitmap()
        {
            //return (System.Drawing.Bitmap)this._Image.Clone();
            return (System.Drawing.Bitmap)this._Image;
        }

        /// <summary>
        /// Returns all available data in formatted string form.
        /// </summary>
        /// <returns>All available data in formatted string form</returns>              
        public override string ToString()
        {
            System.Text.StringBuilder SB = new StringBuilder();

            SB.Append("Image:");
            //SB.Append("\n\tDimensions: " + this.Width + " x " + this.Height + " px");
            SB.Append("\n\tResolution: " + this.XResolution + " x " + this.YResolution + " dpi");
            SB.Append("\n\tOrientation: " + Enum.GetName(typeof(Orientations), this.Orientation));
            SB.Append("\n\tDescription: " + this.ImageDescription);
            SB.Append("\n\tCopyright: " + this.Copyright);
            SB.Append("\nEquipment:");
            SB.Append("\n\tMaker: " + this.Make);
            SB.Append("\n\tModel: " + this.Model);
            SB.Append("\n\tSoftware: " + this.Software);
            SB.Append("\nDate and time:");
            SB.Append("\n\tGeneral: " + this.DateTime.ToString());
            SB.Append("\n\tOriginal: " + this.DateTimeOriginal.ToString());
            SB.Append("\n\tDigitized: " + this.DateTimeDigitized.ToString());
            SB.Append("\nShooting conditions:");
            SB.Append("\n\tExposure time: " + this.ExposureTime.ToString("N4") + " s");
            SB.Append("\n\tExposure program: " + Enum.GetName(typeof(ExposurePrograms), this.ExposureProgram));
            SB.Append("\n\tExposure mode: " + Enum.GetName(typeof(ExposureMeteringModes), this.ExposureMeteringMode));
            SB.Append("\n\tAperture: F" + this.Aperture.ToString("N2"));
            SB.Append("\n\tISO sensitivity: " + this.PhotographicSensitivity);
            SB.Append("\n\tSubject distance: " + this.SubjectDistance.ToString("N2") + " m");
            SB.Append("\n\tFocal length: " + this.FocalLength);
            SB.Append("\n\tFlash: " + Enum.GetName(typeof(Flashs), this.Flash));
            SB.Append("\n\tLight source (WB): " + Enum.GetName(typeof(LightSources), this.LightSource));
            /* To work in Visual Basic: */
            //SB.Replace("\n", vbCrLf);
            //SB.Replace("\t", vbTab);
            return SB.ToString();
        }

        #endregion

        #region Support methods for working with EXIF properties

        /// <summary>
        /// Checks if current image has specified certain property.
        /// </summary>
        /// <param name="propertyID"></param>
        /// <returns>True if image has specified property, False otherwise.</returns>
        public bool IsPropertyDefined(TagNames propertyID)
        {
            return Array.IndexOf(this._Image.PropertyIdList, (int)propertyID) > -1;
        }

        /// <summary>
        /// Gets specified Int16 property.
        /// </summary>
        /// <param name="propertyID">Property ID</param>
        /// <returns>Value of property or 0 if property is not present.</returns>
        public Int16 GetPropertyInt16(TagNames propertyID)
        {
            return GetPropertyInt16(propertyID, 0);
        }

        /// <summary>
        /// Gets specified Int16 property
        /// </summary>
        /// <param name="propertyID">Property ID</param>
        /// <param name="defaultValue">Default value will be returned if property is not present.</param>
        /// <returns>Value of property or defaultValue if property is not present.</returns>
        public Int16 GetPropertyInt16(TagNames propertyID, Int16 defaultValue)
        {
            if (IsPropertyDefined(propertyID))
                return GetInt16(this._Image.GetPropertyItem((int)propertyID).Value);
            else
                return defaultValue;
        }

        /// <summary>
        /// Gets specified Int32 property.
        /// </summary>
        /// <param name="propertyID">Property ID</param>
        /// <returns>Value of property or 0 if property is not present.</returns>
        public Int32 GetPropertyInt32(TagNames propertyID)
        {
            return GetPropertyInt32(propertyID, 0);
        }

        /// <summary>
        /// Gets specified Int32 property.
        /// </summary>
        /// <param name="propertyID">Property ID</param>
        /// <param name="defaultValue">Default value will be returned if property is not present.</param>
        /// <returns>Value of property or defaultValue if property is not present.</returns>
        public Int32 GetPropertyInt32(TagNames propertyID, Int32 defaultValue)
        {
            if (IsPropertyDefined(propertyID))
                return GetInt32(this._Image.GetPropertyItem((int)propertyID).Value);
            else
                return defaultValue;
        }

        /// <summary>
        /// Gets specified Int16 array property.
        /// </summary>
        /// <param name="propertyID">Property ID</param>
        /// <returns>Value of property or an empty array if property is not present.</returns>
        public Int16[] GetPropertyInt16Array(TagNames propertyID)
        {
            return GetPropertyInt16Array(propertyID, new Int16[0]);
        }

        /// <summary>
        /// Gets specified Int16 array property.
        /// </summary>
        /// <param name="propertyID">Property ID</param>
        /// <param name="defaultValue">Default value will be returned if property is not present.</param>
        /// <returns>Value of property or defaultValue if property is not present.</returns>
        public Int16[] GetPropertyInt16Array(TagNames propertyID, Int16[] defaultValue)
        {
            if (IsPropertyDefined(propertyID))
                return GetInt16Array(this._Image.GetPropertyItem((int)propertyID).Value);
            else
                return defaultValue;
        }

        /// <summary>
        /// Gets specified Int32 array property.
        /// </summary>
        /// <param name="propertyID">Property ID</param>
        /// <returns>Value of property or an empty array if property is not present.</returns>
        public Int32[] GetPropertyInt32Array(TagNames propertyID)
        {
            return GetPropertyInt32Array(propertyID, new Int32[0]);
        }

        /// <summary>
        /// Gets specified Int32 array property.
        /// </summary>
        /// <param name="propertyID">Property ID</param>
        /// <param name="defaultValue">Default value will be returned if property is not present.</param>
        /// <returns>Value of property or defaultValue if property is not present.</returns>
        public Int32[] GetPropertyInt32Array(TagNames propertyID, Int32[] defaultValue)
        {
            if (IsPropertyDefined(propertyID))
                return GetInt32Array(this._Image.GetPropertyItem((int)propertyID).Value);
            else
                return defaultValue;
        }      

        /// <summary>
        /// Gets specified string property
        /// </summary>
        /// <param name="propertyID">Property ID</param>
        /// <returns>Value of property or an empty string if property is not present.</returns>
        public string GetPropertyString(TagNames propertyID)
        {
            return GetPropertyString(propertyID, String.Empty);
        }

        /// <summary>
        /// Gets specified string property
        /// </summary>
        /// <param name="propertyID">Property ID</param>
        /// <param name="defaultValue">Default value will be returned if property is not present.</param>
        /// <returns>Value of property or defaultValue if property is not present.</returns>
        public string GetPropertyString(TagNames propertyID, string defaultValue)
        {
            if (IsPropertyDefined(propertyID))
                return GetString(this._Image.GetPropertyItem((int)propertyID).Value);
            else
                return defaultValue;
        }

        /// <summary>
        /// Gets specified rational property
        /// </summary>
        /// <param name="propertyID">Property ID</param>
        /// <returns>Value of property or 0/1 if property is not present.</returns>
        public Rational GetPropertyRational(TagNames propertyID)
        {
            return GetPropertyRational(propertyID, new Rational());
        }

        /// <summary>
        /// Gets specified rational property
        /// </summary>
        /// <param name="propertyID">Property ID</param>
        /// <param name="defaultValue">Default value will be returned if property is not present.</param>
        /// <returns>Value of property or defaultValue if property is not present.</returns>
        public Rational GetPropertyRational(TagNames propertyID, Rational defaultValue)
        {
            if (IsPropertyDefined(propertyID))
                return GetRational(this._Image.GetPropertyItem((int)propertyID).Value);
            else
            {
                Rational R;
                R.Numerator = 0;
                R.Denominator = 1;
                return R;
            }
        }

        /// <summary>
        /// Gets specified property in raw form.
        /// Is recommended to use typed methods instead, when possible.
        /// </summary>
        /// <param name="propertyID">Property ID</param>
        /// <param name="defaultValue">Optional, default Nothing</param>
        /// <returns>Value of property or defaultValue if property is not present.</returns>
        public byte[] GetProperty(TagNames propertyID, byte[] defaultValue = null)
        {
            if (IsPropertyDefined(propertyID))
                return this._Image.GetPropertyItem((int)propertyID).Value;
            else
                return defaultValue;
        }

        /// <summary>
        /// Sets specified Int16 property
        /// </summary>
        /// <param name="property">Property ID</param>
        /// <param name="Value">Value to be set</param>
        public void SetPropertyInt16(TagNames propertyID, Int16 value)
        {
            byte[] Data = new byte[2];
            Data[0] = (byte)(value & 0xFF);
            Data[1] = (byte)((value & 0xFF00) >> 8);
            SetProperty(propertyID, Data, ExifDataTypes.SignedShort);
        }

        /// <summary>
        /// Sets specified Int16 array property
        /// </summary>
        /// <param name="propertyID">Property ID</param>
        /// <param name="value">Value to be set</param>
        public void SetPropertyInt16Array(TagNames propertyID, Int16[] value)
        {
            byte[] Data = new byte[value.Length * 2];
            for (int i = 0; i < value.Length; i++)
            {
                Data[i * 2] = (byte)(value[i] & 0xFF);
                Data[i * 2 + 1] = (byte)((value[i] & 0xFF00) >> 8);
            }
            SetProperty(propertyID, Data, ExifDataTypes.SignedShort);
        }

        /// <summary>
        /// Sets specified Int32 property
        /// </summary>
        /// <param name="propertyID">Property ID</param>
        /// <param name="value">Value to be set</param>
        public void SetPropertyInt32(TagNames propertyID, Int32 value)
        {
            byte[] Data = new byte[4];
            for (int I = 0; I < 4; I++)
            {
                Data[I] = (byte)(value & 0xFF);
                value >>= 8;
            }
            SetProperty(propertyID, Data, ExifDataTypes.SignedLong);
        }

        /// <summary>
        /// Sets specified Int32 array property
        /// </summary>
        /// <param name="propertyID">Property ID</param>
        /// <param name="value">Value to be set</param>
        public void SetPropertyInt32Array(TagNames propertyID, Int32[] value)
        {
            byte[] Data = new byte[value.Length * 4];
            for (int i = 0; i < value.Length; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Data[i * 4 + j] = (byte)(value[i] & 0xFF);
                    value[i] >>= 8;
                }
            }
            SetProperty(propertyID, Data, ExifDataTypes.SignedLong);
        }

        /// <summary>
        /// Sets specified string property
        /// </summary>
        /// <param name="propertyID">Property ID</param>
        /// <param name="value">Value to be set</param>
        public void SetPropertyString(TagNames propertyID, string value)
        {
            byte[] Data = this._Encoding.GetBytes(value + '\0');
            SetProperty(propertyID, Data, ExifDataTypes.AsciiString);
        }

        /// <summary>
        /// Sets specified rational property
        /// </summary>
        /// <param name="propertyID">Property ID</param>
        /// <param name="value">Value to be set</param>
        public void SetPropertyRational(TagNames propertyID, Rational value)
        {
            byte[] Data = new byte[8];
            for (int i = 0; i < 4; i++)
            {
                Data[i] = (byte)(value.Numerator & 0xFF);
                value.Numerator >>= 8;
            }
            for (int i = 4; i < 8; i++)
            {
                Data[i] = (byte)(value.Denominator & 0xFF);
                value.Denominator >>= 8;
            }
            SetProperty(propertyID, Data, ExifDataTypes.SignedRational);
        }

        /// <summary>
        /// Sets specified property in raw form.
        /// Is recommended to use typed methods instead, when possible.
        /// </summary>
        /// <param name="propertyID">Property ID</param>
        /// <param name="Data">Raw data</param>
        /// <param name="Type">EXIF data type</param>
        public void SetProperty(TagNames propertyID, byte[] data, ExifDataTypes type)
        {
            System.Drawing.Imaging.PropertyItem P = this._Image.PropertyItems[0];
            P.Id = (int)propertyID;
            P.Value = data;
            P.Type = (Int16)type;
            P.Len = data.Length;
            try
            {
                this._Image.SetPropertyItem(P);
                this._Image.Save(_FileName);
            }
            catch
            {
                throw new Exception("Set property: " + propertyID + " failed");
            }     
        }

        /// <summary>
        /// Reads Int16 from EXIF bytearray.
        /// </summary>
        /// <param name="bytes">EXIF bytearray to process</param>
        /// <returns>Int16 value</returns>
        private Int16 GetInt16(byte[] bytes)
        {
            if (bytes.Length < 2)
                throw new ArgumentException("Data too short (2 bytes expected)", "bytes");

            return (short)(bytes[1] << 8 | bytes[0]);
        }

        /// <summary>
        /// Reads Int32 from EXIF bytearray.
        /// </summary>
        /// <param name="bytes">EXIF bytearray to process</param>
        /// <returns>Int32 value</returns>
        private Int32 GetInt32(byte[] bytes)
        {
            if (bytes.Length < 4)
                throw new ArgumentException("Data too short (4 bytes expected)", "bytes");

            return bytes[3] << 24 | bytes[2] << 16 | bytes[1] << 8 | bytes[0];
        }

        /// <summary>
        /// Reads Int16 array from EXIF bytearray.
        /// </summary>
        /// <param name="bytes">EXIF bytearray to process</param>
        /// <returns>Int16 array</returns>
        private Int16[] GetInt16Array(byte[] bytes)
        {
            if (bytes.Length < 2)
                throw new ArgumentException("Data too short (2 bytes expected)", "bytes");
            if (bytes.Length % 2 != 0)
            {
                throw new ArgumentException("Format error", "bytes");
            }
            int numOfInt16 = bytes.Length / 2;
            Int16[] array = new Int16[numOfInt16];
            for (int i = 0; i < numOfInt16; i++)
            {
                array[i] = (short)(bytes[i * 2 + 1] << 8 | bytes[i * 2]);
            }
            return array;
        }

        /// <summary>
        /// Reads Int32 array from EXIF bytearray.
        /// </summary>
        /// <param name="bytes">EXIF bytearray to process</param>
        /// <returns>Int32 value</returns>
        private Int32[] GetInt32Array(byte[] bytes)
        {
            if (bytes.Length < 4)
                throw new ArgumentException("Data too short (4 bytes expected)", "bytes");
            if (bytes.Length % 4 != 0)
            {
                throw new ArgumentException("Format error", "bytes");
            }
            int numOfInt32 = bytes.Length / 4;
            Int32[] array = new Int32[numOfInt32];
            for (int i = 0; i < numOfInt32; i++)
            {
                array[i] = bytes[i * 4 + 3] << 24 | bytes[i * 4 + 2] << 16 | bytes[i * 4 + 1] << 8 | bytes[i * 4];
            }
            return array;
        }
 
        /// <summary>
        /// Reads string from EXIF bytearray.
        /// </summary>
        /// <param name="bytes">EXIF bytearray to process</param>
        /// <returns>string value</returns>
        private string GetString(byte[] bytes)
        {
            string R = this._Encoding.GetString(bytes);
            if (R.EndsWith("\0"))
                R = R.Substring(0, R.Length - 1);
            return R;
        }

        /// <summary>
        /// Reads rational from EXIF bytearray.
        /// </summary>
        /// <param name="bytes">EXIF bytearray to process</param>
        /// <returns>rational value</returns>
        private Rational GetRational(byte[] bytes)
        {
            Rational r = new Rational();
            byte[] numerator = new byte[4];
            byte[] denominator = new byte[4];
            Array.Copy(bytes, 0, numerator, 0, 4);
            Array.Copy(bytes, 4, denominator, 0, 4);
            r.Denominator = this.GetInt32(denominator);
            r.Numerator = this.GetInt32(numerator);
            return r;
        }

        #endregion

        public void Dispose()
        {
            this._Image.Dispose();
        }

    }
}
