﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExifSharp
{

    public partial class Exif
    {

        #region Properties

        /// <summary>
        /// Image Width (Readonly)
        /// </summary>
        public Int32 ImageWidth
        {
            get
            {
                return this.GetPropertyInt32(TagNames.ImageWidth);
            }
        }

        /// <summary>
        /// Image Height (Readonly)
        /// </summary>
        public Int32 ImageHeight
        {
            get
            {
                return this.GetPropertyInt32(TagNames.ImageHeight);
            }
        }

        /// <summary>
        /// Number of bits per component (Readonly)
        /// </summary>
        public Int16[] BitsPerSample
        {
            get
            {
                return this.GetPropertyInt16Array(TagNames.BitsPerSample, new short[3] { 8, 8, 8 });
            }
        }

        /// <summary>
        /// Compression scheme (Readonly)
        /// </summary>
        public Compressions Compression
        {
            get
            {
                Int16 X = this.GetPropertyInt16(TagNames.Compression);

                if (Enum.IsDefined(typeof(Compressions), X))
                    return (Compressions)Enum.Parse(typeof(Compressions), Enum.GetName(typeof(Compressions), X));
                else
                    return Compressions.reserved;
            }
        }

        /// <summary>
        /// Pixel Composition (Readonly)
        /// </summary>
        public PhotometricInterpretations PhotometricInterpretation
        {
            get
            {
                Int16 X = this.GetPropertyInt16(TagNames.PhotometricInterpretation);

                if (Enum.IsDefined(typeof(PhotometricInterpretations), X))
                    return (PhotometricInterpretations)Enum.Parse(typeof(PhotometricInterpretations), Enum.GetName(typeof(PhotometricInterpretations), X));
                else
                    return PhotometricInterpretations.reserved;
            }
        }

        /// <summary>
        /// Orientation of image
        /// </summary>
        public Orientations Orientation
        {
            get
            {
                Int16 X = this.GetPropertyInt16(TagNames.Orientation);

                if (!Enum.IsDefined(typeof(Orientations), X))
                    return Orientations.TopLeft;
                else
                    return (Orientations)Enum.Parse(typeof(Orientations), Enum.GetName(typeof(Orientations), X));
            }
            set
            {
                this.SetPropertyInt16(TagNames.Orientation, (short)value);
            }
        }

        /// <summary>
        /// Number of components (Readonly)
        /// </summary>
        public Int16 SamplesPerPixel
        {
            get
            {
                return this.GetPropertyInt16(TagNames.SamplesPerPixel, 3);
            }
        }

        /// <summary>
        /// Image data arrangement
        /// </summary>
        public PlanarConfigurations PlanarConfiguration
        {
            get
            {
                Int16 X = this.GetPropertyInt16(TagNames.PlanarConfiguration);

                if (!Enum.IsDefined(typeof(PlanarConfigurations), X))
                    return PlanarConfigurations.reserved;
                else
                    return (PlanarConfigurations)Enum.Parse(typeof(PlanarConfigurations), Enum.GetName(typeof(PlanarConfigurations), X));
            }
            set
            {
                this.SetPropertyInt16(TagNames.PlanarConfiguration, (short)value);
            }
        }

        /// <summary>
        /// Subsampling ratio of Y to C (Readonly)
        /// </summary>
        public Int16[] YCbCrSubSampling
        {
            get
            {
                return this.GetPropertyInt16Array(TagNames.YCbCrSubSampling);
            }
        }

        /// <summary>
        /// Y and C positioning
        /// </summary>
        public Int16 YCbCrPositioning
        {
            get
            {
                return this.GetPropertyInt16(TagNames.YCbCrPositioning);
            }
            set
            {
                this.SetPropertyInt16(TagNames.YCbCrPositioning, value);
            }
        }


        /// <summary>
        /// Image resolution in width direction
        /// </summary>
        public double XResolution
        {
            get
            {
                double R = this.GetPropertyRational(TagNames.XResolution).ToDouble();

                if (this.GetPropertyInt16(TagNames.ResolutionUnit) == 3)
                {
                    // -- resolution is in points/cm
                    return R * 2.54;
                }
                else
                {
                    // -- resolution is in points/inch
                    return R;
                }
            }
        }

        /// <summary>
        /// Image resolution in height direction
        /// </summary>
        public double YResolution
        {
            get
            {
                double R = this.GetPropertyRational(TagNames.YResolution).ToDouble();

                if (this.GetPropertyInt16(TagNames.ResolutionUnit) == 3)
                {
                    // -- resolution is in points/cm
                    return R * 2.54;
                }
                else
                {
                    // -- resolution is in points/inch
                    return R;
                }
            }
        }

        /// <summary>
        /// Unit of X and Y resolution
        /// </summary>
        public Int16 ResolutionUnit
        {
            get
            {
                return this.GetPropertyInt16(TagNames.ResolutionUnit);
            }
            set
            {
                this.SetPropertyInt16(TagNames.ResolutionUnit, value);
            }
        }

        /// <summary>
        /// Image data location (Readonly)
        /// </summary>
        public Int32[] StripOffsets
        {
            get
            {
                return this.GetPropertyInt32Array(TagNames.StripOffsets);
            }
        }

        /// <summary>
        /// Number of rows per strip (Readonly)
        /// </summary>
        public Int32 RowsPerStrip
        {
            get
            {
                return this.GetPropertyInt32(TagNames.RowsPerStrip);
            }
        }

        /// <summary>
        /// Bytes per compressed strip (Readonly)
        /// </summary>
        public Int32[] StripBytesCounts
        {
            get
            {
                return this.GetPropertyInt32Array(TagNames.StripBytesCounts);
            }
        }

        /// <summary>
        /// Offset to JPEG SOI (Readonly)
        /// </summary>
        public Int32 JPEGInterchangeFormat
        {
            get
            {
                return this.GetPropertyInt32(TagNames.JPEGInterchangeFormat);
            }
            set
            {
                try
                {
                    this.SetPropertyInt32(TagNames.JPEGInterchangeFormat, value);
                }
                catch { }
            }
        }

        /// <summary>
        /// Bytes of JPEG data (Readonly)
        /// </summary>
        public Int32 JPEGInterchangeFormatLength
        {
            get
            {
                return this.GetPropertyInt32(TagNames.JPEGInterchangeFormatLength);
            }
            set
            {
                try
                {
                    this.SetPropertyInt32(TagNames.JPEGInterchangeFormatLength, value);
                }
                catch { }
            }
        }
        
        /// <summary>
        /// Transfer function
        /// </summary>
        public Int16[] TransferFuncition
        {
            get
            {
                return this.GetPropertyInt16Array(TagNames.TransferFuncition);
            }
            set
            {
                try
                {
                    this.SetPropertyInt16Array(TagNames.TransferFuncition, value);
                }
                catch { }
            }
        }
        
        /// <summary>
        /// White point chromaticity
        /// </summary>
        public Rational WhitePoint
        {
            get
            {
                return this.GetPropertyRational(TagNames.WhitePoint);
            }
            set
            {
                try
                {
                    this.SetPropertyRational(TagNames.WhitePoint, value);
                }
                catch { }
            }
        }

        /// <summary>
        /// Chromaticities of primaries
        /// </summary>
        public Rational PrimaryChromaticities
        {
            get
            {
                return this.GetPropertyRational(TagNames.PrimaryChromaticities);
            }
            set
            {
                try
                {
                    this.SetPropertyRational(TagNames.PrimaryChromaticities, value);
                }
                catch { }
            }
        }

        /// <summary>
        /// Color space transformation matrix coefficients
        /// </summary>
        public Rational YCbCrCoefficients
        {
            get
            {
                return this.GetPropertyRational(TagNames.YCbCrCoefficients);
            }
            set
            {
                this.SetPropertyRational(TagNames.YCbCrCoefficients, value);
            }
        }

        /// <summary>
        /// Pair of black and white reference values
        /// </summary>
        public Rational ReferenceBlackWhite
        {
            get
            {
                return this.GetPropertyRational(TagNames.ReferenceBlackWhite);
            }
            set
            {
                this.SetPropertyRational(TagNames.ReferenceBlackWhite, value);
            }
        }
        
        /// <summary>
        /// File change date and time
        /// </summary>
        public DateTime DateTime
        {
            get
            {
                try
                {
                    return DateTime.ParseExact(this.GetPropertyString(TagNames.DateTime), @"yyyy\:MM\:dd HH\:mm\:ss", null);
                }
                catch
                {
                    return DateTime.MinValue;
                }
            }
            set
            {
                this.SetPropertyString(TagNames.DateTime, value.ToString(@"yyyy\:MM\:dd HH\:mm\:ss"));
            }
        }

        /// <summary>
        /// Image title
        /// </summary>
        public string ImageDescription
        {
            get
            {
                return this.GetPropertyString(TagNames.ImageDescription);
            }
            set
            {
                this.SetPropertyString(TagNames.ImageDescription, value);
            }
        }

        /// <summary>
        /// Image input equipment manufacturer
        /// </summary>
        public string Make
        {
            get
            {
                return this.GetPropertyString(TagNames.Make);
            }
            set
            {
                this.SetPropertyString(TagNames.Make, value);
            }
        }

        /// <summary>
        /// Image input equipment model
        /// </summary>               
        public string Model
        {
            get
            {
                return this.GetPropertyString(TagNames.Model);
            }
            set
            {
                this.SetPropertyString(TagNames.Model, value);
            }
        }

        /// <summary>
        /// Software used
        /// </summary>
        public string Software
        {
            get
            {
                return this.GetPropertyString(TagNames.Software);
            }
            set
            {
                this.SetPropertyString(TagNames.Software, value);
            }
        }
        
        /// <summary>
        /// Person who created the image
        /// </summary>
        public string Artist
        {
            get
            {
                return this.GetPropertyString(TagNames.Artist);
            }
            set
            {
                this.SetPropertyString(TagNames.Artist, value);
            }
        }
        
        /// <summary>
        /// Copyright holder
        /// </summary>
        public string Copyright
        {
            get
            {
                return this.GetPropertyString(TagNames.Copyright);
            }
            set
            {
                this.SetPropertyString(TagNames.Copyright, value);
            }
        }
        
        #endregion
    }
}
