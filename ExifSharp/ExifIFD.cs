﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExifSharp
{

    public partial class Exif
    {
        /// <summary>
        /// 
        /// </summary>
        public string ExifVersion
        {
            get
            {
                return this.GetPropertyString(TagNames.ExifVersion);
            }
            set
            {
                this.SetPropertyString(TagNames.ExifVersion, value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string FlashpixVersion
        {
            get
            {
                return this.GetPropertyString(TagNames.FlashpixVersion);
            }
            set
            {
                this.SetPropertyString(TagNames.FlashpixVersion, value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 ColorSpace
        {
            get
            {
                return this.GetPropertyInt16(TagNames.ColorSpace);
            }
            set
            {
                this.SetPropertyInt16(TagNames.ColorSpace, value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Rational Gamma
        {
            get
            {
                return this.GetPropertyRational(TagNames.Gamma);
            }
            set
            {
                this.SetPropertyRational(TagNames.Gamma, value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string ComponentsConfiguration
        {
            get
            {
                return this.GetPropertyString(TagNames.ComponentsConfiguration);
            }
            set
            {
                this.SetPropertyString(TagNames.ComponentsConfiguration, value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Rational CompressedBitsPerPixel
        {
            get
            {
                return this.GetPropertyRational(TagNames.CompressedBitsPerPixel);
            }
            set
            {
                this.SetPropertyRational(TagNames.CompressedBitsPerPixel, value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 PixelXDimension
        {
            get
            {
                return this.GetPropertyInt16(TagNames.PixelXDimension);
            }
            set
            {
                this.SetPropertyInt16(TagNames.PixelXDimension, value);
            }
        }
        //
        //
        public Int16 PixelYDimension
        {
            get
            {
                return this.GetPropertyInt16(TagNames.PixelYDimension);
            }
            set
            {
                this.SetPropertyInt16(TagNames.PixelYDimension, value);
            }
        }
        //
        //
        public string MakerNote
        {
            get
            {
                return this.GetPropertyString(TagNames.MakerNote);
            }
            set
            {
                this.SetPropertyString(TagNames.MakerNote, value);
            }
        }
        //
        //
        public string UserComment
        {
            get
            {
                return this.GetPropertyString(TagNames.UserComment);
            }
            set
            {
                this.SetPropertyString(TagNames.UserComment, value);
            }
        }
        //
        //
        public string RelatedSoundFile
        {
            get
            {
                return this.GetPropertyString(TagNames.RelatedSoundFile);
            }
            set
            {
                this.SetPropertyString(TagNames.RelatedSoundFile, value);
            }
        }
        // 
        // 拍摄时间
        // Time when image was taken (EXIF DateTimeOriginal).
        // 
        public DateTime DateTimeOriginal
        {
            get
            {
                try
                {
                    return DateTime.ParseExact(this.GetPropertyString(TagNames.DateTimeOriginal), @"yyyy\:MM\:dd HH\:mm\:ss", null);
                }
                catch
                {
                    return DateTime.MinValue;
                }
            }
            set
            {
                this.SetPropertyString(TagNames.DateTimeOriginal, value.ToString(@"yyyy\:MM\:dd HH\:mm\:ss"));
            }
        }
        // 
        // 数字化时间
        // Time when image was digitized (EXIF DateTimeDigitized).
        // 
        public DateTime DateTimeDigitized
        {
            get
            {
                try
                {
                    return DateTime.ParseExact(this.GetPropertyString(TagNames.DateTimeDigitized), @"yyyy\:MM\:dd HH\:mm\:ss", null);
                }
                catch
                {
                    return DateTime.MinValue;
                }
            }
            set
            {
                this.SetPropertyString(TagNames.DateTimeDigitized, value.ToString(@"yyyy\:MM\:dd HH\:mm\:ss"));
            }
        }
        //
        //
        public string SubSecTime
        {
            get
            {
                return this.GetPropertyString(TagNames.SubSecTime);
            }
            set
            {
                this.SetPropertyString(TagNames.SubSecTime, value);
            }
        }
        //
        //
        public string SubSecTimeOriginal
        {
            get
            {
                return this.GetPropertyString(TagNames.SubSecTimeOriginal);
            }
            set
            {
                this.SetPropertyString(TagNames.SubSecTimeOriginal, value);
            }
        }
        //
        //
        public string SubSecTimeDigitized
        {
            get
            {
                return this.GetPropertyString(TagNames.SubSecTimeDigitized);
            }
            set
            {
                this.SetPropertyString(TagNames.SubSecTimeDigitized, value);
            }
        }
        // 
        // 曝光时间（秒）
        // Exposure time in seconds (EXIF ExifExposureTime/ExifShutterSpeed)
        // 
        public double ExposureTimeAbs
        {
            get
            {
                if (this.IsPropertyDefined(TagNames.ExposureTime))
                    // -- Exposure time is explicitly specified
                    return this.GetPropertyRational(TagNames.ExposureTime).ToDouble();
                else
                    if (this.IsPropertyDefined(TagNames.ShutterSpeedValue))
                    //'-- Compute exposure time from shutter spee 
                    return (1 / Math.Pow(2, this.GetPropertyRational(TagNames.ShutterSpeedValue).ToDouble()));
                else
                    // -- Can't figure out 
                    return 0;
            }
            set
            {

            }
        }

        public Rational ExposureTime
        {
            get
            {
                if (this.IsPropertyDefined(TagNames.ExposureTime))
                    // -- Exposure time is explicitly specified
                    return this.GetPropertyRational(TagNames.ExposureTime);
                else
                    return new Rational();
            }
            set
            {

            }
        }

        // 
        // 光圈（F）
        // Aperture value as F number (EXIF ExifFNumber/ExifApertureValue)
        // 
        public double Aperture
        {
            get
            {
                if (this.IsPropertyDefined(TagNames.FNumber))
                    return this.GetPropertyRational(TagNames.FNumber).ToDouble();
                else
                    if (this.IsPropertyDefined(TagNames.ApertureValue))
                    return Math.Pow(System.Math.Sqrt(2), this.GetPropertyRational(TagNames.ApertureValue).ToDouble());
                else
                    return 0;
            }
            set
            {

            }
        }
        // 
        // 曝光程序
        // Exposure program used (EXIF ExifExposureProg)
        // 
        // If not specified, returns Normal (2)
        // 
        public ExposurePrograms ExposureProgram
        {
            get
            {
                Int16 X = this.GetPropertyInt16(TagNames.ExposureProgram);

                if (Enum.IsDefined(typeof(ExposurePrograms), X))
                    return (ExposurePrograms)Enum.Parse(typeof(ExposurePrograms), Enum.GetName(typeof(ExposurePrograms), X));
                else
                    return ExposurePrograms.Normal;
            }
            set
            {

            }
        }
        //
        //
        public string SpectralSensitivity
        {
            get
            {
                return this.GetPropertyString(TagNames.SpectralSensitivity);
            }
            set
            {
                this.SetPropertyString(TagNames.SpectralSensitivity, value);
            }
        }
        // ISO
        // ISO
        public Int16 PhotographicSensitivity
        {
            get
            {
                return this.GetPropertyInt16(TagNames.PhotographicSensitivity);
            }
            set
            {
                this.SetPropertyInt16(TagNames.PhotographicSensitivity, value);
            }
        }
        //
        //
        public string OECF
        {
            get
            {
                return this.GetPropertyString(TagNames.OECF);
            }
            set
            {
                this.SetPropertyString(TagNames.OECF, value);
            }
        }
        //
        //
        public Int16 SensitivityType
        {
            get
            {
                return this.GetPropertyInt16(TagNames.SensitivityType);
            }
            set
            {
                this.SetPropertyInt16(TagNames.SensitivityType, value);
            }
        }
        //
        //
        public Int32 StandardOutputSensitivity
        {
            get
            {
                return this.GetPropertyInt32(TagNames.StandardOutputSensitivity);
            }
            set
            {
                this.SetPropertyInt32(TagNames.StandardOutputSensitivity, value);
            }
        }
        //
        //
        public Int32 RecommendExposureIndex
        {
            get
            {
                return this.GetPropertyInt32(TagNames.RecommendExposureIndex);
            }
            set
            {
                this.SetPropertyInt32(TagNames.RecommendExposureIndex, value);
            }
        }
        //
        //
        public Int32 ISOSpeed
        {
            get
            {
                return this.GetPropertyInt32(TagNames.ISOSpeed);
            }
            set
            {
                this.SetPropertyInt32(TagNames.ISOSpeed, value);

            }
        }
        //
        //
        public Int32 ISOSpeedLatitudeyyy
        {
            get
            {
                return this.GetPropertyInt32(TagNames.ISOSpeedLatitudeyyy);
            }
            set
            {
                this.SetPropertyInt32(TagNames.ISOSpeedLatitudeyyy, value);
            }
        }
        //
        //
        public Int32 ISOSpeedLatitudezzz
        {
            get
            {
                return this.GetPropertyInt32(TagNames.ISOSpeedLatitudezzz);
            }
            set
            {
                this.SetPropertyInt32(TagNames.ISOSpeedLatitudezzz, value);
            }
        }
        //
        //
        public Rational ShutterSpeedValue
        {
            get
            {
                return this.GetPropertyRational(TagNames.ShutterSpeedValue);
            }
            set
            {
                this.SetPropertyRational(TagNames.ShutterSpeedValue, value);
            }
        }
        //
        //
        public Rational ApertureValue
        {
            get
            {
                return this.GetPropertyRational(TagNames.ApertureValue);
            }
            set
            {
                this.SetPropertyRational(TagNames.ApertureValue, value);
            }
        }
        //
        //
        public Rational BrightnessValue
        {
            get
            {
                return this.GetPropertyRational(TagNames.BrightnessValue);
            }
            set
            {
                this.SetPropertyRational(TagNames.BrightnessValue, value);
            }
        }
        //
        //
        public Rational ExposureBiasValue
        {
            get
            {
                return this.GetPropertyRational(TagNames.ExposureBiasValue);
            }
            set
            {
                this.SetPropertyRational(TagNames.ExposureBiasValue, value);
            }
        }
        //
        //
        public Rational MaxApertureValue
        {
            get
            {
                return this.GetPropertyRational(TagNames.MaxApertureValue);
            }
            set
            {
                this.SetPropertyRational(TagNames.MaxApertureValue, value);
            }
        }
        //
        //
        public Rational SubjectDistance
        {
            get
            {
                return this.GetPropertyRational(TagNames.SubjectDistance);
            }
            set
            {
                this.SetPropertyRational(TagNames.SubjectDistance, value);
            }
        }
        // 曝光测量模式
        // Exposure method metering mode used (EXIF MeteringMode)
        // 
        // If not specified, returns Unknown (0)
        // 
        public ExposureMeteringModes ExposureMeteringMode
        {
            get
            {
                Int16 X = this.GetPropertyInt16(TagNames.MeteringMode);

                if (Enum.IsDefined(typeof(ExposureMeteringModes), X))
                    return (ExposureMeteringModes)Enum.Parse(typeof(ExposureMeteringModes), Enum.GetName(typeof(ExposureMeteringModes), X));
                else
                    return ExposureMeteringModes.Unknown;
            }
            set
            {
                this.SetPropertyInt32(TagNames.MeteringMode, (short)value);
            }
        }
        // 
        // 光源/白平衡
        // Light source / white balance (EXIF LightSource)
        // 
        // If not specified, returns Unknown (0).
        // 
        public LightSources LightSource
        {
            get
            {
                Int16 X = this.GetPropertyInt16(TagNames.LightSource);

                if (Enum.IsDefined(typeof(LightSources), X))
                    return (LightSources)Enum.Parse(typeof(LightSources), Enum.GetName(typeof(LightSources), X));
                else
                    return LightSources.Unknown;
            }
            set
            {
                this.SetPropertyInt32(TagNames.LightSource, (short)value);
            }
        }
        // 
        // 闪光模式
        // Flash mode (EXIF Flash)
        // 
        // If not present, value NotFired (0) is returned
        // 
        public Flashs Flash
        {
            get
            {
                Int16 X = this.GetPropertyInt16(TagNames.Flash);

                if (Enum.IsDefined(typeof(Flashs), X))
                    return (Flashs)Enum.Parse(typeof(Flashs), Enum.GetName(typeof(Flashs), X));
                else
                    return Flashs.NotFired;
            }
            set
            {
                this.SetPropertyInt16(TagNames.Flash, (short)value);
            }
        }
        // 
        // 焦距
        // Focal length of lenses in mm (EXIF FocalLength)
        // 
        public Rational FocalLength
        {
            get
            {
                return this.GetPropertyRational(TagNames.FocalLength);
            }
            set
            {
                this.SetPropertyRational(TagNames.FocalLength, value);
            }
        }
        //
        //
        public Int16 SubjectArea
        {
            get
            {
                return this.GetPropertyInt16(TagNames.SubjectArea);
            }
            set
            {
                this.SetPropertyInt16(TagNames.SubjectArea, value);
            }
        }
        //
        //
        public Rational FlashEnergy
        {
            get
            {
                return this.GetPropertyRational(TagNames.FlashEnergy);
            }
            set
            {
                this.SetPropertyRational(TagNames.FlashEnergy, value);
            }
        }
        //
        //
        public string SpatialFrequencyResponse
        {
            get
            {
                return this.GetPropertyString(TagNames.SpatialFrequencyResponse);
            }
            set
            {
                this.SetPropertyString(TagNames.SpatialFrequencyResponse, value);
            }
        }
        //
        //
        public Rational FocalPlaneXResolution
        {
            get
            {
                return this.GetPropertyRational(TagNames.FocalPlaneXResolution);
            }
            set
            {
                this.SetPropertyRational(TagNames.FocalPlaneXResolution, value);
            }
        }
        //
        //
        public Rational FocalPlaneYResolution
        {
            get
            {
                return this.GetPropertyRational(TagNames.FocalPlaneYResolution);
            }
            set
            {
                this.SetPropertyRational(TagNames.FocalPlaneYResolution, value);
            }
        }
        //
        //
        public Int16 FocalPlaneResolutionUnit
        {
            get
            {
                return this.GetPropertyInt16(TagNames.FocalPlaneResolutionUnit);
            }
            set
            {
                this.SetPropertyInt16(TagNames.FocalPlaneResolutionUnit, value);
            }
        }
        //
        //
        public Int16 SubjectLocation
        {
            get
            {
                return this.GetPropertyInt16(TagNames.SubjectLocation);
            }
            set
            {
                this.SetPropertyInt16(TagNames.SubjectLocation, value);
            }
        }
        //
        //
        public Rational ExposureIndex
        {
            get
            {
                return this.GetPropertyRational(TagNames.ExposureIndex);
            }
            set
            {
                this.SetPropertyRational(TagNames.ExposureIndex, value);
            }
        }
        //
        //
        public Int16 SensingMethod
        {
            get
            {
                return this.GetPropertyInt16(TagNames.SensingMethod);
            }
            set
            {
                this.SetPropertyInt16(TagNames.SensingMethod, value);
            }
        }
        //
        //
        public string FileSource
        {
            get
            {
                return this.GetPropertyString(TagNames.FileSource);
            }
            set
            {
                this.SetPropertyString(TagNames.FileSource, value);
            }
        }
        //
        //
        public string SceneType
        {
            get
            {
                return this.GetPropertyString(TagNames.SceneType);
            }
            set
            {
                this.SetPropertyString(TagNames.SceneType, value);
            }
        }
        //
        //
        public string CFAPattern
        {
            get
            {
                return this.GetPropertyString(TagNames.CFAPattern);
            }
            set
            {
                this.SetPropertyString(TagNames.CFAPattern, value);
            }
        }
        //
        //
        public Int16 CustomRendered
        {
            get
            {
                return this.GetPropertyInt16(TagNames.CustomRendered);
            }
            set
            {
                this.SetPropertyInt16(TagNames.CustomRendered, value);
            }
        }
        //
        //
        public Int16 ExposureMode
        {
            get
            {
                return this.GetPropertyInt16(TagNames.ExposureMode);
            }
            set
            {
                this.SetPropertyInt16(TagNames.ExposureMode, value);
            }
        }
        //
        //
        public Int16 WhiteBalance
        {
            get
            {
                return this.GetPropertyInt16(TagNames.WhiteBalance);
            }
            set
            {
                this.SetPropertyInt16(TagNames.WhiteBalance, value);
            }
        }
        //
        //
        public Rational DigitalZoomRatio
        {
            get
            {
                return this.GetPropertyRational(TagNames.DigitalZoomRatio);
            }
            set
            {
                this.SetPropertyRational(TagNames.DigitalZoomRatio, value);
            }
        }
        //
        //
        public Int16 FocalLengthIn35mmFilm
        {
            get
            {
                return this.GetPropertyInt16(TagNames.FocalLengthIn35mmFilm);
            }
            set
            {
                this.SetPropertyInt16(TagNames.FocalLengthIn35mmFilm, value);
            }
        }
        //
        //
        public Int16 SceneCaptureType
        {
            get
            {
                return this.GetPropertyInt16(TagNames.SceneCaptureType);
            }
            set
            {
                this.SetPropertyInt16(TagNames.SceneCaptureType, value);
            }
        }
        //
        //
        public Rational GainControl
        {
            get
            {
                return this.GetPropertyRational(TagNames.GainControl);
            }
            set
            {
                this.SetPropertyRational(TagNames.GainControl, value);
            }
        }
        //
        //
        public Int16 Contrast
        {
            get
            {
                return this.GetPropertyInt16(TagNames.Contrast);
            }
            set
            {
                this.SetPropertyInt16(TagNames.Contrast, value);
            }
        }
        //
        //
        public Int16 Saturation
        {
            get
            {
                return this.GetPropertyInt16(TagNames.Saturation);
            }
            set
            {
                this.SetPropertyInt16(TagNames.Saturation, value);
            }
        }
        //
        //
        public Int16 Sharpness
        {
            get
            {
                return this.GetPropertyInt16(TagNames.Sharpness);
            }
            set
            {
                this.SetPropertyInt16(TagNames.Sharpness, value);
            }
        }
        //
        //
        public string DeviceSettingDescription
        {
            get
            {
                return this.GetPropertyString(TagNames.DeviceSettingDescription);
            }
            set
            {
                this.SetPropertyString(TagNames.DeviceSettingDescription, value);
            }
        }
        //
        //
        public Int16 SubjectDistanceRange
        {
            get
            {
                return this.GetPropertyInt16(TagNames.SubjectDistanceRange);
            }
            set
            {
                this.SetPropertyInt16(TagNames.SubjectDistanceRange, value);
            }
        }
        //
        // 图像ID
        // Unique image ID
        //
        public string ImageUniqueID
        {
            get
            {
                return this.GetPropertyString(TagNames.ImageUniqueID);
            }
            set
            {
                this.SetPropertyString(TagNames.ImageUniqueID, value);
            }
        }
        //
        //
        public string CameraOwnerName
        {
            get
            {
                return this.GetPropertyString(TagNames.CameraOwnerName);
            }
            set
            {
                this.SetPropertyString(TagNames.CameraOwnerName, value);
            }
        }
        //
        //
        public string BodySerialNumber
        {
            get
            {
                return this.GetPropertyString(TagNames.BodySerialNumber);
            }
            set
            {
                this.SetPropertyString(TagNames.BodySerialNumber, value);
            }
        }
        //
        //
        public Rational LensSpecification
        {
            get
            {
                return this.GetPropertyRational(TagNames.LensSpecification);
            }
            set
            {
                this.SetPropertyRational(TagNames.LensSpecification, value);
            }
        }
        // 
        // 镜头制造商
        // Lens make
        // 
        public string LensMake
        {
            get
            {
                return this.GetPropertyString(TagNames.LensMake);
            }
            set
            {
                this.SetPropertyString(TagNames.LensMake, value);
            }
        }

        // 
        // 镜头型号
        // Lens model
        // 
        public string LensModel
        {
            get
            {
                return this.GetPropertyString(TagNames.LensModel);
            }
            set
            {
                this.SetPropertyString(TagNames.LensModel, value);
            }
        }
        //
        //
        public string LensSerialNumber
        {
            get
            {
                return this.GetPropertyString(TagNames.LensSerialNumber);
            }
            set
            {
                this.SetPropertyString(TagNames.LensSerialNumber, value);
            }
        }
    }
}
