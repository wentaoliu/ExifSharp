﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ExifSharp;

namespace Demo
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Exif exif = new Exif(@"d:\exif_demo.jpg");

            txtImageWidth.Text = exif.ImageWidth.ToString();
            txtImageHeight.Text = exif.ImageHeight.ToString();
            txtBitsPerSample.Text = string.Join(",", exif.BitsPerSample);

            Array compressions = Enum.GetValues(typeof(Exif.Compressions));
            cboCompression.ItemsSource = compressions;
            cboCompression.SelectedValue = exif.Compression;

            Array photometricInterpretations = Enum.GetValues(typeof(Exif.PhotometricInterpretations));
            cboPhotometricInterpretation.ItemsSource = photometricInterpretations;
            cboPhotometricInterpretation.SelectedValue = exif.PhotometricInterpretation;

            Array orientations = Enum.GetValues(typeof(Exif.Orientations));
            cboOrientation.ItemsSource = orientations;
            cboOrientation.SelectedValue = exif.Orientation;

            txtSamplesPerPixel.Text = exif.SamplesPerPixel.ToString();

            Array planarConfigurations = Enum.GetValues(typeof(Exif.PlanarConfigurations));
            cboPlanarConfiguration.ItemsSource = planarConfigurations;
            cboPlanarConfiguration.SelectedValue = exif.PlanarConfiguration;

            txtYCbCrSubSampling.Text = string.Join(",", exif.YCbCrSubSampling);
            txtYCbCrPositioning.Text = exif.YCbCrPositioning.ToString();
            txtXResolution.Text = exif.XResolution.ToString();
            txtYResolution.Text = exif.YResolution.ToString();
            txtStripOffsets.Text = string.Join(",", exif.StripOffsets);
            txtRowsPerStrip.Text = exif.RowsPerStrip.ToString();
            txtStripBytesCounts.Text = string.Join(",", exif.StripBytesCounts);
            txtJPEGInterchangeFormat.Text = exif.JPEGInterchangeFormat.ToString();
            txtJPEGInterchangeFormatLength.Text = exif.JPEGInterchangeFormatLength.ToString();
            txtTransferFuncition.Text = string.Join(",", exif.TransferFuncition);
            txtWhitePoint.Text = exif.WhitePoint.ToString();
            txtPrimaryChromaticities.Text = exif.PrimaryChromaticities.ToString();
            txtYCbCrCoefficients.Text = exif.YCbCrCoefficients.ToString();
            txtReferenceBlackWhite.Text = exif.ReferenceBlackWhite.ToString();
            txtDateTime.Text = exif.DateTimeDigitized.ToString();
            txtImageDescription.Text = exif.ImageDescription;
            txtMake.Text = exif.Make;
            txtModel.Text = exif.Model;
            txtSoftware.Text = exif.Software;
            txtArtist.Text = exif.Artist;
            txtCopyright.Text = exif.Artist;
            txtExifVersion.Text = exif.ExifVersion;
            txtFlashpixVersion.Text = exif.FlashpixVersion;
            txtColorSpace.Text = exif.ColorSpace.ToString();
            txtGamma.Text = exif.Gamma.ToString();
            txtComponentsConfiguration.Text = exif.ComponentsConfiguration;
            txtCompressedBitsPerPixel.Text = exif.CompressedBitsPerPixel.ToDouble().ToString();
            txtPixelXDimension.Text = exif.PixelXDimension.ToString();
            txtPixelYDimension.Text = exif.PixelYDimension.ToString();
            txtMakerNote.Text = exif.MakerNote;
            txtUserComment.Text = exif.UserComment;
            txtRelatedSoundFile.Text = exif.RelatedSoundFile;
            txtDateTimeOriginal.Text = exif.DateTimeDigitized.ToString();
            txtDateTimeDigitized.Text = exif.DateTimeOriginal.ToString();
            txtSubSecTime.Text = exif.SubSecTime;
            txtSubSecTimeOriginal.Text = exif.SubSecTimeOriginal;
            txtSubSecTimeDigitized.Text = exif.SubSecTimeDigitized;
            txtExposureTime.Text = exif.ExposureTimeAbs.ToString();
            txtAperture.Text = "F" + exif.Aperture;

            Array exposurePrograms = Enum.GetValues(typeof(Exif.ExposurePrograms));
            cboExposureProgram.ItemsSource = exposurePrograms;
            cboExposureProgram.SelectedValue = exif.ExposureProgram;

            txtSpectralSensitivity.Text = exif.SpectralSensitivity;
            txtPhotographicSensitivity.Text = exif.PhotographicSensitivity.ToString();
            txtOECF.Text = exif.OECF;
            txtSensitivityType.Text = exif.SensitivityType.ToString();
            txtStandardOutputSensitivity.Text = exif.StandardOutputSensitivity.ToString();
            txtRecommendExposureIndex.Text = exif.RecommendExposureIndex.ToString();
            txtISOSpeed.Text = exif.ISOSpeed.ToString();
            txtISOSpeedLatitudeyyy.Text = exif.ISOSpeedLatitudeyyy.ToString();
            txtISOSpeedLatitudezzz.Text = exif.ISOSpeedLatitudezzz.ToString();
            txtShutterSpeedValue.Text = exif.ShutterSpeedValue.ToString();
            txtApertureValue.Text = exif.ApertureValue.ToString();
            txtBrightnessValue.Text = exif.BrightnessValue.ToString();
            txtExposureBiasValue.Text = exif.BrightnessValue.ToString();
            txtMaxApertureValue.Text = exif.MaxApertureValue.ToString();
            txtSubjectDistance.Text = exif.SubjectDistance.ToString();

            Array exposureMeteringModes = Enum.GetValues(typeof(Exif.ExposureMeteringModes));
            cboExposureMeteringModes.ItemsSource = exposureMeteringModes;
            cboExposureMeteringModes.SelectedValue = exif.ExposureMeteringMode;

            Array lightSources = Enum.GetValues(typeof(Exif.LightSources));
            cboLightSource.ItemsSource = lightSources;
            cboLightSource.SelectedValue = exif.LightSource;

            Array flashs = Enum.GetValues(typeof(Exif.Flashs));
            cboFlash.ItemsSource = flashs;
            cboFlash.SelectedValue = exif.Flash;

            txtFocalLength.Text = exif.FocalLength.ToDouble().ToString();
            txtSubjectArea.Text = exif.SubjectArea.ToString();
            txtFlashEnergy.Text = exif.FlashEnergy + "bcps";
            txtSpatialFrequencyResponse.Text = exif.SpatialFrequencyResponse;
            txtFocalPlaneXResolution.Text = exif.FocalPlaneXResolution.ToDouble().ToString();
            txtFocalPlaneYResolution.Text = exif.FocalPlaneYResolution.ToDouble().ToString();
            txtFocalPlaneResolutionUnit.Text = exif.FocalPlaneResolutionUnit.ToString();
            txtSubjectLocation.Text = exif.SubjectLocation.ToString();
            txtExposureIndex.Text = exif.ExposureIndex.ToString();
            txtSensingMethod.Text = exif.SensingMethod.ToString();
            txtFileSource.Text = exif.FileSource;
            txtSceneType.Text = exif.SceneType;
            txtCFAPattern.Text = exif.CFAPattern;
            txtCustomRendered.Text = exif.CustomRendered.ToString();
            txtExposureMode.Text = exif.ExposureMode.ToString();
            txtWhiteBalance.Text = exif.WhiteBalance.ToString();
            txtDigitalZoomRatio.Text = exif.DigitalZoomRatio.ToString();
            txtFocalLengthIn35mmFilm.Text = exif.FocalLengthIn35mmFilm.ToString();
            txtSceneCaptureType.Text = exif.SceneCaptureType.ToString();
            txtGainControl.Text = exif.GainControl.ToString();
            txtContrast.Text = exif.Contrast.ToString();
            txtSaturation.Text = exif.Saturation.ToString();
            txtSharpness.Text = exif.Sharpness.ToString();
            txtDeviceSettingDescription.Text = exif.DeviceSettingDescription;
            txtSubjectDistanceRange.Text = exif.SubjectDistanceRange.ToString();
            txtImageUniqueID.Text = exif.ImageUniqueID.ToString();
            txtCameraOwnerName.Text = exif.CameraOwnerName;
            txtBodySerialNumber.Text = exif.BodySerialNumber;
            txtLensSpecification.Text = exif.LensSpecification.ToString();
            txtLensMake.Text = exif.LensMake;
            txtLensModel.Text = exif.LensModel;
            txtLensSerialNumber.Text = exif.LensSerialNumber;
        }
    }
}
